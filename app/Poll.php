<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $fillable = ['question'];
    protected $options_add = [];
    protected $maxSelection = 1;
    /**
     * A poll has many options related to
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(Option::class);
    }
    public function getAllOptions($id)
    {
        $allpictures = $this->options()->where('poll_id', $id)->get();
        foreach($allpictures as $pic){
            $currPath[] = $pic;
        }
        return $currPath;
    }

    public function countVotes($id)
    {
        $allVotes = $this->options()->where('poll_id', $id)->get();
        $votes_sum = null;
        foreach($allVotes as $vote){
            $votes_sum += $vote->votes;
        }
        return $votes_sum;
    }

    public function optionsNumber()
    {
        return $this->options()->count();
    }

    /**
     * Creating new Poll
     *
     * @return App\Poll;
     */
    public static function createFromRequest($request)
    {
        $poll = new Poll([
            'question' => $request['question']
        ]);
        $poll->addOptions($request['options']);
        if(array_key_exists('maxCheck', $request)){
            $poll->maxSelection($request['maxCheck']);
        }
        $poll->generate();
        return $poll;
    }

    public function maxSelection($number = 1)
    {
        if($number <= 1){
            $number = 1;
        }
        $this->maxSelection = $number;
        return $this;
    }

    public function addOptions($options)
    {
        if(is_array($options))
        {
            foreach($options as $option){
                if(is_string($option)){
                    $this->pushOption($option);
                }else{
                    throw new \InvalidArgumentException("Array arguments must be composed of Strings values");
                }
            }
            return $this;
        }
        if(is_string($options)){
            $this->pushOption($options);
            return $this;
        }
        throw new \InvalidArgumentException("Invalid Argument provided");
    }

    private function pushOption($option)
    {
        if(! in_array($option, $this->options_add)){
            $this->options_add[] = $option;
            return true;
        }
        return false;
    }
    public function generate()
    {
        $totalOptions = count($this->options_add);
        // No option add yet
        if($totalOptions == 0)
            throw new OptionsNotProvidedException();
        // There must be 2 options at least
        if($totalOptions == 1 )
            throw new OptionsInvalidNumberProvidedException();
        // At least one options should not be selected
        if($totalOptions <= $this->maxSelection )
            throw new CheckedOptionsException();
        // Create Poll && assign options to it
        \DB::transaction(function () {
            $this->maxCheck = $this->maxSelection;
            $this->save();
            $this->options()
                ->saveMany($this->instantiateOptions());
        });
        return true;
    }

    private function instantiateOptions()
    {
        $options = [];
        foreach($this->options_add as $option){
            $options[] = new Option([
                'name' => $option
            ]);
        }
        return $options;
    }

    public function attach($options)
    {
        
        if(is_array($options))
        {
            $newOptions = [];
            foreach($options as $option){
                if(is_string($option)){
                    $newOptions[] = new Option([
                        'name' => $option
                    ]);
                }else{
                    throw new \InvalidArgumentException("Array arguments must be composed of Strings values");
                }
            }
            return $this->options()->saveMany($newOptions);
        }
        if(is_string($options)){
            return $this->options()->save(
                new Option([
                    'name' => $options
                ])
            );
        }
        throw new \InvalidArgumentException("Invalid Argument provided");
    }

    /**
     * Remove a (list of elements)
     *
     * @param $options
     * @return bool
     * @throws CheckedOptionsException
     * @throws InvalidArgumentException
     */
    public function detach($options)
    {
        // Prepare the elements as an array
        $options = is_array($options) ? $options : func_get_args();
        $oldOptions = [];
        $elements = $this->options()->pluck('id');
        foreach($options as $option){
            if(is_numeric($option) && is_int(intval($option))){
                $option = intval($option);
                $option = Option::findOrFail($option);
                if($this->containsAndNotVoted($elements, $option->id)){
                    $oldOptions[] = $option->id;
                }
            }else if($option instanceof Option){
                if($this->containsAndNotVoted($elements, $option->getKey())) {
                    $oldOptions = $option->getKey();
                }
            }else {
                throw new \InvalidArgumentException("Array arguments must be composed of ids or option object values");
            }
        }
        // verify the number of options
        
        $diff = ($elements->count() - count($oldOptions));
        if($this->isRadio()){
            $count = count($oldOptions);
            return Option::destroy($oldOptions) == $count;
        }
        // checkbox case
        if($diff <= $this->maxCheck)
            throw new CheckedOptionsException();
        $count = count($oldOptions);
        return Option::destroy($oldOptions) == $count;
    }

    private function containsAndNotVoted($elements, $against)
    {
        return $elements->contains($against);
    }
    public function isRadio()
    {
        return $this->maxCheck == 1;
    }

    protected $poll;
    /**
     * Select poll
     *
     * @param Poll $poll
     * @return $this
     */
    public function poll(Poll $poll)
    {
        $this->poll = $poll;
        return $this;
    }
    public function results()
    {
        $this->results = collect();
        foreach($this->options()->get() as $option){
            $this->results->push([
              "option" => $option,
              "votes" => $option->countVotes(),
            ]);
        }
        return $this;
    }
    public function grab()
    {
        if(! is_null($this->results)){
            return $this->results->toArray();
        }
    }
}