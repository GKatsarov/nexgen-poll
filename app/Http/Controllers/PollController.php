<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;

class PollController extends Controller
{
    /**
     * Show all the Polls in the database
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $polls = Poll::withCount('options')->get();
        return view('polls.dashboard', compact('polls'));
    }
    /**
     * Store the Request
     *
     * @param PollCreationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $poll = Poll::createFromRequest($request->all());
        return redirect(route('poll.index'))
            ->with('success', 'Your poll has been addedd successfully');
    }
    /**
     * Show the poll to be prepared to edit
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Poll $poll)
    {
        return view('polls.edit', compact('poll'));
    }
    /**
     * Update the Poll
     *
     * @param Poll $poll
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Poll $poll, Request $request)
    {
        $poll->update($request->all());

        return redirect(route('poll.index'))
            ->with('success', 'Your poll has been updated successfully');
    }
    /**
     * Delete a Poll
     *
     * @param Poll $poll
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Poll $poll)
    {
        $poll->options()->delete();
        $poll->delete();
        return redirect(route('poll.index'))
            ->with('success', 'Your poll has been deleted successfully');
    }
    public function create()
    {
        return view('polls.create');
    }

    public function show($id){
        $poll = Poll::findOrFail($id);
        $total = $poll->countVotes($id);

        return view('polls.show_results')
            ->with('poll',$poll)
            ->with('total',$total);
    }
    public function getVotesView($id){
        $poll = Poll::findOrFail($id);

        return view('polls.vote')
            ->with('poll',$poll);
    }
}
