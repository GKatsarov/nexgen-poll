<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use App\Poll;

class OptionController extends Controller
{
    public function add(Poll $poll,Request $request)
    {
        $poll->attach($request->get('options'));
        return redirect(route('poll.index'))
            ->with('success', 'New poll options have been added successfully');
    }
    public function push(Poll $poll)
    {
        return view('options.edit', compact('poll'));
    }
    /**
     * Remove the Selected Option
     *
     * @param Poll $poll
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Poll $poll, Request $request)
    {
        try{
            $poll->detach($request->get('options'));
            return redirect(route('poll.index'))
                ->with('success', 'Poll options have been removed successfully');
        }catch (\Exception $e){
            $message = Poll::getMessage($e);
            return back()
                ->withErrors($message);
        }
    }
    public function delete(Poll $poll)
    {
        return view('options.delete', compact('poll'));
    }

    /**
     * Make a Vote
     *
     * @param Poll $poll
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function vote(Request $request, $id)
    {
        $option = Option::findOrFail($request->get('options'));
        $newVotes = $option->votes+1;

        $option->votes = $newVotes;
        $option->save();

        return redirect(route('poll.show', $id));
    }
}
