@extends('templates.base')
@section('title')
    Polls- Edit
@endsection
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('poll.index') }}">Polls</a></li>
            <li class="active">Edit Poll</li>
        </ol>
        <div class="well col-md-8 col-md-offset-2">
            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form method="POST" action=" {{ route('poll.update', $poll->id) }}">
                <input name="_method" type="hidden" value="PUT">
                {{ csrf_field() }}
                <!-- Question Input -->
                <div class="form-group">
                    <label><h4>Current Pool Question:&nbsp;{{ $poll->question }}</h4></label>
                </div>
                <div class="form-group">
                    <label><h5>Rename:</h5></label>
                    <input name="question" value="{{ $poll->question }}" class="form-control"/>
                </div>
                <!-- Create Form Submit -->
                <div class="form-group">
                    <div class="col-sm-6">
                        <input name="update" type="submit" value="Update" class="btn btn-primary form-control"/>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('poll.index') }}" class="btn btn-default form-control">Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection