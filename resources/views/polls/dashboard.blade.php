@extends('templates.base')
@section('style')
    <style>
        .table td, .table th {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li class="active"><a href="{{ route('poll.index') }}">Polls</a></li>
        </ol>
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if($polls->count() >= 1)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Options</th>
                    <th>Current Votes</th>
                    <th>Vote</th>
                    <th>View Results</th>
                    <th>Edit</th>
                    <th>Add Options</th>
                    <th>Remove Options</th>
                    <th>Remove Poll</th>
                </tr>
                </thead>
                <tbody>
                @forelse($polls as $poll)
                    <tr>
                        <th scope="row">{{ $poll->id }}</th>
                        <td><a href="{{ route('poll.vote', $poll->id) }}">{{ $poll->question }}</a></td>
                        <td>{{ $poll->options_count }}</td>
                        <td>{{ $poll->countVotes($poll->id) }}</td>
                        <td>
                            <a class="btn btn-default btn-sm" href="{{ route('poll.vote', $poll->id) }}">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-default btn-sm" href="{{ route('poll.show', $poll->id) }}">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-default btn-sm" href="{{ route('poll.edit', $poll->id) }}">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-default btn-sm" href="{{ route('poll.options.add', $poll->id) }}">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-default btn-sm" href="{{ route('poll.options.remove', $poll->id) }}">
                                <i class="fa fa-minus-circle" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <form action="{{ route('poll.destroy', $poll->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-sm">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <smal>No poll has been found. Try to add one <a href="{{ route('poll.create') }}">Now.</a></smal>
        @endif
    </div>
@endsection