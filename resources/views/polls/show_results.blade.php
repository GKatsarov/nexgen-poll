@extends('templates.base')
@section('style')
    <style>
        .table td, .table th {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('poll.index') }}">Polls</a></li>
            <li class="active">View current votes</li>
        </ul>
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="col-sm-12">
        <div class="row">
        <div class="col-sm-10">
            <h3>
                <strong>Poll: {{$poll->question}}</strong>
            </h3>
        </div>
            <div class="col-sm-2">
                <a href="{{ route('poll.index') }}" class="btn btn-primary pull-right">Back</a>
            </div>
        </div>
            @foreach($poll->getAllOptions($poll->id) as $option)
            @php
            $currVotes = $option->votes;
            if($total == 0){
                $percent = 0;
            }else{
                $percent = ($currVotes * 100) /($total);
            }
            @endphp
            <div class='result-option-id'>
                <strong>{{$option->name}}</strong><span class='pull-right'>{{round($percent, 0)}}%</span>
                <div class='progress'>
                    <div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='{{round($percent, 0)}}' aria-valuemin='0' aria-valuemax='100' style='width: {{round($percent, 0)}}%'>
                        <span class='sr-only'>{{round($percent, 0)}}% Complete</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
    </div>
@endsection