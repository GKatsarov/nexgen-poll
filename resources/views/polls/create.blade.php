@extends('templates.base')
@section('title')
    Polls- Creation
@endsection
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('poll.index') }}">Polls</a></li>
            <li class="active">Create New Poll</li>
        </ol>
    <div class="well col-md-8 col-md-offset-2">
            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        <form method="POST" action=" {{ route('poll.store') }}">
            {{ csrf_field() }}
            <!-- Question Input -->
            <div class="col-sm-12" id="create_poll_form">
                <label for="question" class="col-md-2 control-label">Question:</label>
                <div class="col-md-10 form-group">
                <input id="question" type="text" name="question" class="form-control" required/>
                </div>
                <label for="options[0]" class="col-md-2 control-label">Option 1:</label>
                <div class="col-md-10 form-group">
                    <input type="text" name="options[0]" class="form-control" required/>
                </div>
                <label for="options[1]" class="col-md-2 control-label">Option 2:</label>
                <div class="col-md-10 form-group">
                    <input type="text" name="options[1]" class="form-control" required/>
                </div>
            </div>
            <!-- Create Form Submit -->
            <div class="form-group col-sm-3 pull-right">
                <input name="create" type="submit" value="Create" class="btn btn-primary form-control"/>
            </div>
        </form>
            <a class="btn btn-success" id="add">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
            </a>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        function remove(current){
            debugger;
            $('#label'+current.parentNode.id).remove();
            current.parentNode.remove();
        }
        var $counter = 3;
        document.getElementById("add").onclick = function() {
            // create label
            var e1 = document.createElement('label');
            e1.className = ('col-md-2 control-label');
            e1.id = ('label'+$counter);
            e1.innerText = ('Option'+$counter+':');
            //create input field
            var e = document.createElement('div');
            e.className = ('col-md-10 form-group');
            e.id= ($counter);
            e.innerHTML = "<input type='text' name='options[]' class='form-control add-input' required /> <a class='btn btn-danger' href='#' onclick='remove(this)'><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
            //append document
            document.getElementById("create_poll_form").appendChild(e1);
            document.getElementById("create_poll_form").appendChild(e);
            //increment counter
            $counter = $counter+1;
        }
    </script>
@endsection