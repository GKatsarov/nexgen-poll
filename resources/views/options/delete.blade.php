@extends('templates.base')
@section('title')
    Polls- Remove options
@endsection
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('poll.index') }}">Polls</a></li>
            <li class="active">Remove Options</li>
        </ol>
        <div class="well col-md-8 col-md-offset-2">
            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form method="POST" action=" {{ route('poll.options.remove', $poll->id) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <!-- Question Input -->
                <div class="form-group">
                    <label>{{ $poll->question }}</label>
                    <div class="radio">
                        @foreach($poll->options as $option)
                            <label>
                                <input type="checkbox" name="options[]" value={{ $option->id }}> {{ $option->name }}
                            </label>
                            <br/>
                        @endforeach
                    </div>
                </div>
                <!-- Create Form Submit -->
                <div class="form-group">
                    <div class="col-sm-6">
                        <input name="Delete" type="submit" value="Delete" class="btn btn-danger form-control" >
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('poll.index') }}" class="btn btn-default form-control">Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection