I've uploaded both the vendor and the .env file, so it would be possibly easier to run the project.

You should configure the .env file to ur needs in database and possibly database.php in config folder again with your schema name.
In order for the migrations to roll you need to configure the database name and run the "php artisan migrate" command.

After that it should be possible to run the command "php artisan serve" - laravel command similar to apache functions, in the repo folder. 
It will boot a server to 127.0.0.1 where u will be able to see the site.

You should now be able to create/edit/delete polls, as well as add/remove multiple options. Also vote for a poll and see the poll's results.