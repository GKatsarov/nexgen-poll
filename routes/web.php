<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Polls
Route::resource('/poll', 'PollController');

//Options
Route::get('poll/{poll}/options/add', ['uses' => 'OptionController@push', 'as' => 'poll.options.push']);
Route::post('poll/{poll}/options/add', ['uses' => 'OptionController@add', 'as' => 'poll.options.add']);
Route::get('poll/{poll}/options/remove', ['uses' => 'OptionController@delete', 'as' => 'poll.options.remove']);
Route::delete('poll/{poll}/options/remove', ['uses' => 'OptionController@remove', 'as' => 'poll.options.remove']);

//Vote
Route::post('/poll/vote/{poll}', ['uses' => 'OptionController@vote', 'as' => 'poll.vote']);
Route::get('poll/vote/{poll}',['uses' => 'PollController@getVotesView', 'as' => 'poll.vote']);

//Dashboard redirect
Route::get('/', function () {
    return redirect(route('poll.index'));
});